use data_a_la_case;
-- Créer un produit “Truc”, avec comme description “Lorem Ipsum” comme image “https://picsum.photos/200”, qui coûte 200 €.
INSERT INTO product VALUES(NULL, 'Truc', 'Lorem Ipsum', 'https://picsum.photos/200', '200 €', true);

-- Créer un produit “Bidule”, avec comme description “Lorem merol” comme image “https://picsum.photos/100”, qui coûte 500 €.
INSERT INTO product VALUES(NULL, 'Bidule', 'Lorem merol', 'https://picsum.photos/100', '500 €', true);

-- Récupérer la liste des produits et vérifier le prix de Truc et de Bidule.
SELECT * FROM product;

-- Récupérer Truc à partir de son id et vérifier sa description et son image.
SELECT * FROM product WHERE name = 'Truc';

-- Créer le client “Bob” avec comme mail “bob@gmail.com”.
INSERT INTO Client (name, email) VALUES ('Bob', 'bob@gmail.com');

-- Mettre à jour Bob pour ajouter son numéro de téléphone “0123456789”.
UPDATE Client SET phone = '0123456789' WHERE name = 'Bob';

-- Créer l’adresse “12 street St, 12345 Schenectady, New York, US”
SELECT * FROM address;
INSERT INTO address (street, city, zipcode, state, country) 
VALUES ('12 street St', 'Schenectady', 12345, 'New York', 'US');

-- Associer cette adresse à Bob.
INSERT INTO client_address (client_id, address_id) VALUES (1, 1);

-- Créer l’adresse “12 boulevard de Strasbourg, 31000, Toulouse, OC, France”
INSERT INTO address (street, city, zipcode, state, country) 
VALUES ('12 boulevard de Strasbourg', 'Toulouse', 31000, 'OC', 'France');

-- Créer une nouvelle commande.
SELECT * FROM address;
INSERT INTO `order` (client_id, delivery_address, billing_address, payment_method, order_state)
VALUES (1, 1, 2, 'credit_card', 'cart');

-- Associer Bob à la commande
UPDATE `order` SET client_id = 1 WHERE id = 1;

-- Essayer d’associer cette adresse comme facturation à la commande.
UPDATE `order` SET billing_address = 1 WHERE id = 1;

-- Associer cette adresse comme facturation à la commande.
UPDATE `order` SET billing_address = 1 WHERE id = 1;

-- Ajouter 3 Trucs à la commande.
INSERT INTO order_line (product_id, order_id, qt) VALUES (1, 1, 3);

-- Ajouter 1 Bidules à la commande.
INSERT INTO order_line (product_id, order_id, qt) VALUES (2, 1, 1);

-- Associer cette adresse à Bob.
INSERT INTO client_address (client_id, address_id) VALUES (1, 2);

-- Associer cette adresse comme adresse de livraison.
UPDATE `order` SET delivery_address = 2 WHERE id = 1;

-- Essayer de passer la commande dans l’état suivant (vérifier qu’il y a une erreur).

-- Associer le moyen de paiement “Card” à la commande.
INSERT INTO payment_method (credit_card) VALUES (true);
UPDATE `order` SET payment_method = 1 WHERE id = 1;

-- Passer la commande dans l’état suivant.
UPDATE `order` SET order_state = 'validated' WHERE id = 1;

-- Passer la commande dans l’état suivant.
UPDATE `order` SET order_state = 'sent' WHERE id = 1;

-- Vérifier que la commande est dans l’état “sent”.
SELECT * FROM `order` WHERE order_state = 'sent';

-- Récupérer la liste des adresses de Bob.
SELECT a.* FROM address a
JOIN client_address ca ON a.id = ca.address_id
WHERE ca.client_id = 1;

-- Récupérer la liste des commandes de Bob.
SELECT * FROM `order` WHERE client_id = 1;
