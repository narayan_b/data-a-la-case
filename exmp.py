import mysql.connector
import pandas as pd
from fastapi import FastAPI

app = FastAPI()

# Establish the database connection
connection = mysql.connector.connect(
    host="localhost",
    user="narayan23",
    password="coucou123",
    auth_plugin='mysql_native_password',
    database='data_a_la_case'
)

# Create a cursor for executing SQL queries
cursor = connection.cursor()

# Example: Run a simple query to check connection
cursor.execute("SELECT 1;")
result = cursor.fetchone()

# Function to execute SQL queries and return results as JSON
def execute_query(query):
    cursor.execute(query)
    columns = [col[0] for col in cursor.description]
    data = cursor.fetchall()
    return [dict(zip(columns, row)) for row in data]


# Get all the elements from the table
@app.get("/")
def read_root():
    return {"message": "Hello, World!"}

@app.get("/get_address")
def get_address():
    query = 'SELECT * FROM address;'
    return execute_query(query)

@app.get("/get_client")
def get_client():
    query = 'SELECT * FROM Client;'
    return execute_query(query)

@app.get("/get_order")
def get_order():
    query = 'SELECT * FROM `order`;'
    return execute_query(query)

@app.get("/get_product")
def get_product():
    query = 'SELECT * FROM product;'
    return execute_query(query)

@app.get("/get_payment_method")
def get_payment_method():
    query = 'SELECT * FROM payment_method;'
    return execute_query(query)

@app.get("/get_order_state")
def get_order_state():
    query = 'SELECT * FROM order_state;'
    return execute_query(query)

@app.get("/get_order_line")
def get_order_line():
    query = 'SELECT * FROM order_line;'
    return execute_query(query)


# Insert data in the table

@app.post("/add_address")
def add_address(address: dict):
    query = f"INSERT INTO address (street, city, zipcode, state, country) VALUES ('{address['street']}', '{address['city']}', {address['zipcode']}, '{address['state']}', '{address['country']}');"
    return execute_query(query)

@app.post("/add_client")
def add_client(client: dict):
    query = f"INSERT INTO Client (name, email, phone, default_address) VALUES ('{client['name']}', '{client['email']}', '{client['phone']}', {client['default_address']});"
    return execute_query(query)

@app.post("/add_order")
def add_order(order: dict):
    query = f"INSERT INTO `order` (client_id, delivery_address, billing_address, payment_method, order_state) VALUES ({order['client_id']}, {order['delivery_address']}, {order['billing_address']}, '{order['payment_method']}', '{order['order_state']}');"
    return execute_query(query)

@app.post("/add_product")
def add_product(product: dict):
    query = f"INSERT INTO product (name, description, images, price, available) VALUES ('{product['name']}', '{product['description']}', '{product['images']}', '{product['price']}', {product['available']});"
    return execute_query(query)

@app.post("/add_payment_method")
def add_payment_method(payment_method: dict):
    query = f"INSERT INTO payment_method (credit_card, bank_check_on_delivery, cash_on_delivery) VALUES ({payment_method['credit_card']}, {payment_method['bank_check_on_delivery']}, {payment_method['cash_on_delivery']});"
    return execute_query(query)

@app.post("/add_order_state")
def add_order_state(order_state: dict):
    query = f"INSERT INTO order_state (cart, validated, sent, delivery) VALUES ({order_state['cart']}, {order_state['validated']}, {order_state['sent']}, {order_state['delivery']});"
    return execute_query(query)

@app.post("/add_order_line")
def add_order_line(order_line: dict):
    query = f"INSERT INTO order_line (product_id, order_id, qt) VALUES ({order_line['product_id']}, {order_line['order_id']}, {order_line['qt']});"
    return execute_query(query)


# Update data in the table
@app.put("/update_address/{address_id}")
def update_address(address_id: int, updated_address: dict):
    query = f"UPDATE address SET street = '{updated_address['street']}', city = '{updated_address['city']}', zipcode = {updated_address['zipcode']}, state = '{updated_address['state']}', country = '{updated_address['country']}' WHERE id = {address_id};"
    return execute_query(query)

@app.put("/update_client/{client_id}")
def update_client(client_id: int, updated_client: dict):
    query = f"UPDATE Client SET name = '{updated_client['name']}', email = '{updated_client['email']}', phone = '{updated_client['phone']}', default_address = {updated_client['default_address']} WHERE id = {client_id};"
    return execute_query(query)

@app.put("/update_order/{order_id}")
def update_order(order_id: int, updated_order: dict):
    query = f"UPDATE `order` SET client_id = {updated_order['client_id']}, delivery_address = {updated_order['delivery_address']}, billing_address = {updated_order['billing_address']}, payment_method = '{updated_order['payment_method']}', order_state = '{updated_order['order_state']}' WHERE id = {order_id};"
    return execute_query(query)

@app.put("/update_product/{product_id}")
def update_product(product_id: int, updated_product: dict):
    query = f"UPDATE product SET name = '{updated_product['name']}', description = '{updated_product['description']}', images = '{updated_product['images']}', price = '{updated_product['price']}', available = {updated_product['available']} WHERE id = {product_id};"
    return execute_query(query)

@app.put("/update_payment_method/{payment_method_id}")
def update_payment_method(payment_method_id: int, updated_payment_method: dict):
    query = f"UPDATE payment_method SET credit_card = {updated_payment_method['credit_card']}, bank_check_on_delivery = {updated_payment_method['bank_check_on_delivery']}, cash_on_delivery = {updated_payment_method['cash_on_delivery']} WHERE id = {payment_method_id};"
    return execute_query(query)

@app.put("/update_order_state/{order_state_id}")
def update_order_state(order_state_id: int, updated_order_state: dict):
    query = f"UPDATE order_state SET cart = {updated_order_state['cart']}, validated = {updated_order_state['validated']}, sent = {updated_order_state['sent']}, delivery = {updated_order_state['delivery']} WHERE id = {order_state_id};"
    return execute_query(query)

@app.put("/update_order_line/{order_line_id}")
def update_order_line(order_line_id: int, updated_order_line: dict):
    query = f"UPDATE order_line SET product_id = {updated_order_line['product_id']}, order_id = {updated_order_line['order_id']}, qt = {updated_order_line['qt']} WHERE id = {order_line_id};"
    return execute_query(query)



# Delete the elements from the table
@app.delete("/delete_address/{address_id}")
def delete_address(address_id: int):
    query = f'DELETE FROM address WHERE id = {address_id};'
    return execute_query(query)

@app.delete("/delete_client/{client_id}")
def delete_client(client_id: int):
    query = f'DELETE FROM Client WHERE id = {client_id};'
    return execute_query(query)

@app.delete("/delete_order/{order_id}")
def delete_order(order_id: int):
    query = f'DELETE FROM `order` WHERE id = {order_id};'
    return execute_query(query)

@app.delete("/delete_product/{product_id}")
def delete_product(product_id: int):
    query = f'DELETE FROM product WHERE id = {product_id};'
    return execute_query(query)

@app.delete("/delete_payment_method/{payment_method_id}")
def delete_payment_method(payment_method_id: int):
    query = f'DELETE FROM payment_method WHERE id = {payment_method_id};'
    return execute_query(query)

@app.delete("/delete_order_state/{order_state_id}")
def delete_order_state(order_state_id: int):
    query = f'DELETE FROM order_state WHERE id = {order_state_id};'
    return execute_query(query)

@app.delete("/delete_order_line/{order_line_id}")
def delete_order_line(order_line_id: int):
    query = f'DELETE FROM order_line WHERE id = {order_line_id};'
    return execute_query(query)



if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="127.0.0.1", port=8000, reload=True)
