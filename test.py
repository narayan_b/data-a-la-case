# # Créer une variable compte avec une valeur de 0.0.
# a = 0.0

# # Créer une fonction operation(n) qui modifie la valeur de compte en ajoutant n.
# def operation(n):
#     global a
#     a = a + n
#     if a < 0:
#         raise ValueError("Solde négatif détecté")

# # Faire une opération à +100.
# operation(100)
# print("Solde actuel:", a)

# # Faire une opération à -30.
# operation(-30)
# print("Solde actuel:", a)

# # Modifier la fonction operation pour qu’elle émette une erreur si le solde devrait être négatif.
# # Sans faire la mise à jour du solde !

# # Faire une opération à -100.
# try:
#     operation(-100)
#     print("Solde actuel:", a)
# except ValueError as e:
#     print("Erreur:", e)
#     print("Pas assez de sous")

# # Prévenez l’opération précédente pour éviter que l’erreur ne mette fin au programme.
# # Afficher “pas assez de sous” en cas d’erreur.



# solde = 0.0
# DA = 100
# solde2 = 0
# DA2 = 200

# def operation(n: float):
#     global solde
#     if solde + n >= -DA:
#         solde += n
#         return solde
#     else:
#         return 'pas assez de sous'


# # test

# print(operation(100))
# print(operation(-10))
# print(operation(5))
# print(operation(-1000))
# print(operation(+10))
# print("Je dois avoir 105")
# print(operation(-150))
# print('avec mon DA de 100 je suis à -45')
# print(operation(-100))
# print(solde)
# print('je suis toujours à -45')



# class Compte(object):
#     solde: float
#     DA: float
#     nom: str

#     def __init__(self, nom, DA):
#         self.nom = nom
#         self.solde = DA
#         self.DA = 0.0

#     def operation(self, n: float):
#         if self.solde + n >= -self.DA:
#             self.solde += n
#             return self.solde
#         else:
#             return 'pas assez de sous'


# # tests
# # créer le compte de bob
# compte_de_bob = Compte('Bob')
# # donner 10 balles à bob
# print(compte_de_bob.operation(10))
# print(compte_de_bob.operation(100))
# print(compte_de_bob.operation(-150))



# class Compte(object):
#     solde: float
#     DA: float
#     nom: str

#     def __init__(self, nom):
#         self.nom = nom
#         self.solde = 0.0
#         self.DA = 0.0
#         print(f"le compte de {self.nom} est créé !")

#     def operation(self, n: float):
#         if self.solde + n >= -self.DA:
#             self.solde += n
#             print(f"le solde de {self.nom} est de {self.solde}")
#             return self.solde
#         else:
#             print(f"{self.nom} n'a pas assez de sous pour faire cette opération : {n}")
#             return self.solde

#     def set_DA(self, n: float):
#         self.DA = n
#         print(f"le découvert autorisé de {self.nom} est de {self.DA}")

#     def __str__(self):
#         return f"Compte de {self.nom} : Solde = {self.solde} ; DA = {self.DA}"





# if __name__ == '__main__':
#     # tests
    
#     class Mouton:
#         nom : str
#         numero = 0

#         def __init__(self, nom):
#             self.nom = nom
#             Mouton.numero += 1
#             self.numero = Mouton.numero

#         def crie(self):
#             return 'Bhéééééééé'

#         def __str__(self):
#             return f"Je m'appelle {self.nom} et je suis un mouton."

# shaun = Mouton('Shaun')
# print(shaun.crie())
# print(shaun)

# # Créer un autre mouton qui s'appelle Kebab
# kebab = Mouton('Kebab')
# print(kebab)

# # Créer un troupeau de 138 moutons
# troupeau = [Mouton(f'Mouton-{i}') for i in range(1, 139)]

# # Ajouter Shaun et Kebab au troupeau
# troupeau.append(Mouton('Shaun'))
# troupeau.append(kebab)

# # Attaque de loup !
# # Faire crier tout le troupeau
# for mouton in troupeau:
#     print(f"Mouton {mouton.numero}: {mouton.crie()}")

import random
import string

lettres = string.ascii_lowercase
f = [a + b for a in lettres for b in lettres]
print(len(f))
print(f[:26])
g = [a + b + c for a in lettres for b in lettres for c in lettres]
print(len(g))
print(g[:26])
h = {clef: random.randint(2, 12) for clef in g}
print(len(h))
moyenne = sum(h.values()) / len(h)
print(moyenne)