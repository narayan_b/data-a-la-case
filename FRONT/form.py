import dash
from dash import html, dcc, callback, Output, Input, State
import dash_bootstrap_components as dbc
import requests

# Assuming you have a Dash app instance
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP], suppress_callback_exceptions=True)

# Steps
steps = [
    {"id": "step1", "name": "Client Details", "fields": ["client-name", "client-email", "client-phone"]},
    {"id": "step2", "name": "Delivery, Billing, Payment", "fields": ["delivery-address", "billing-address", "payment-method"]},
    {"id": "step3", "name": "Order Details", "fields": ["product-name", "product-description", "product-price", "product-quantity", "order-state"]},
    {"id": "confirmation", "name": "Order Confirmation", "fields": []},
]

# Layout
app.layout = html.Div([
    dcc.Location(id='url', refresh=True),  # Change refresh to True
    html.Div(id='page-content'),
    html.Button("Next", id="btn-next", n_clicks=0),
    html.Button("Submit Order", id="btn-submit-order", n_clicks=0),
    html.Div(id='order-confirmation')  # New Div for order confirmation
])

# Callback to switch between steps and submit order
@app.callback([Output('order-confirmation', 'children'),
               Output('page-content', 'children')],
              [Input('url', 'pathname'),
               Input('btn-next', 'n_clicks'),
               Input('btn-submit-order', 'n_clicks')],
              [State(f, 'value') for step in steps for f in step["fields"]],
              prevent_initial_call=True)
def process_steps(pathname, n_clicks_next, n_clicks_submit, *form_data):
    print("Triggered ID:", dash.callback_context.triggered_id)
    print("Form Data:", form_data)

    triggered_id = dash.callback_context.triggered_id
    if triggered_id:
        # Determine which button was clicked
        if 'btn-next' in triggered_id:
            return None, steps_layout(get_next_step(pathname))
        elif 'btn-submit-order' in triggered_id:
            # Here you can send the form_data to your API using the requests library
            api_url = "your_api_endpoint"
            data = {field: value for field, value in zip([f for step in steps for f in step["fields"]], form_data)}
            print("Sending data to API:", data)
            
            response = requests.post(api_url, json=data)

            # Display confirmation message
            confirmation_message = html.Div([
                html.H1("Order Submitted Successfully"),
                html.P(f"API Response: {response.text}")
            ])

            return confirmation_message, None
    return None, steps_layout(get_next_step(pathname))


# Helper function to get next step
def get_next_step(pathname):
    current_step_index = get_step_index(pathname)
    if current_step_index is not None:
        next_step_index = current_step_index + 1
        if next_step_index < len(steps):
            return steps[next_step_index]
        elif current_step_index == len(steps) - 1:
            return steps[-1]  # Confirmation step
    return steps[0]

# Helper function to get step index from pathname
def get_step_index(pathname):
    for i, step in enumerate(steps):
        if pathname.endswith(f'/{step["id"]}'):
            return i
    return None

# Helper function to generate layout for each step
def steps_layout(step):
    layout = html.Div([
        html.H1(f"Step {steps.index(step) + 1}: {step['name']}"),
    ])

    for field_id in step["fields"]:
        if field_id == "client-name":
            layout.children.append(dcc.Input(id=field_id, type="text", placeholder=field_id.replace("-", " ").title(), value=""))
        else:
            layout.children.append(dcc.Input(id=field_id, type="text", placeholder=field_id.replace("-", " ").title()))

    return layout

if __name__ == "__main__":
    app.run_server(debug=True)
