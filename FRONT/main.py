import dash
from dash import dcc, html, Input, Output
import requests
from dash.dependencies import Input, Output, State

app = dash.Dash(__name__)

# Define FastAPI routes
base_url = "http://127.0.0.1:8000"
product_url = f"{base_url}/product"

# Fetch product data from FastAPI
products = requests.get(product_url).json()

# Initial cart state
cart = []

app.layout = html.Div([
    html.H1("Product List"),
    html.Div(id='product-list'),
    html.Div([
        html.Div(id='cart-info'),
        html.Button("Open Cart", id='open-cart-button'),
    ], style={'float': 'right'}),
])


# Callback to update product list
@app.callback(
    Output('product-list', 'children'),
    Input('open-cart-button', 'n_clicks')
)


def update_product_list(n_clicks):
    product_list = []

    for i, product in enumerate(products):
        product_list.append(
            html.Div(
                [
                    html.Div(
                        [
                            html.Img(src=product['images'], alt=product['name'], style={'width': '100%', 'border-radius': '25px'}),
                            html.H3(product['name'], style={'margin-top': '10px'}),
                            html.P(f"Description: {product['description']}"),
                            html.P(f"Price: {product['price']}"),
                            html.Button(
                                f"Add to Cart",
                                id={'type': 'add-to-cart', 'index': product['id']},
                                n_clicks=0,
                                style={
                                    'margin-top': '10px',
                                    'background-color': '#4CAF50',
                                    'color': 'white',
                                    'padding': '10px 20px',
                                    'border': 'none',
                                    'text-align': 'center',
                                    'text-decoration': 'none',
                                    'display': 'inline-block',
                                    'font-size': '16px',
                                    'border-radius': '5px',
                                },
                            ),
                        ],
                        style={'border': '1px solid #ddd', 'box-shadow': '0px 2px 6px 1px rgba(0, 0, 255, .2)', 'border-radius': '5px', 'padding': '20px', 'margin': '10px', 'text-align': 'center'},
                    )
                ],
                style={'display': 'inline-block', 'width': '30%', 'margin': '10px'},
            )
        )

    rows = [product_list[i:i + 3] for i in range(0, len(product_list), 3)]
    final_layout = html.Div([html.Div(row) for row in rows])

    return final_layout

# Callback to handle adding products to the cart
@app.callback(
    Output('cart-info', 'children'),
    Output('open-cart-button', 'children'),
    Input({'type': 'add-to-cart', 'index': 'n_clicks'}, 'n_clicks'),
    State({'type': 'add-to-cart', 'index': 'product_id'}, 'value'),
    prevent_initial_call=True
)
def add_to_cart_callback(n_clicks, product_id):
    ctx = dash.callback_context

    if not ctx.triggered_id:
        raise dash.exceptions.PreventUpdate

    global cart
    cart.append(product_id)

    cart_info = f"Cart ({len(cart)} items)"
    open_cart_button = f"Open Cart ({len(cart)} items)"

    return cart_info, open_cart_button

if __name__ == '__main__':
    app.run_server(debug=True)
