import mysql.connector
from faker import Faker
import random
from typing import List
from fastapi import FastAPI
from fastapi.responses import JSONResponse

app = FastAPI()

# Establish the database connection and cursor
connection = mysql.connector.connect(
    host="localhost",
    user="narayan23",
    password="coucou123",
    auth_plugin='mysql_native_password',
    database='data_a_la_case'
)

cursor = connection.cursor()

# Create a Faker instance
fake = Faker()

# Insert 10 fake elements into the 'address' table
for _ in range(10):
    cursor.execute("INSERT INTO address (street, city, zipcode, state, country) VALUES (%s, %s, %s, %s, %s)",
                   (fake.street_address(), fake.city(), fake.zipcode(), fake.state(), fake.country()))

# Insert 10 fake elements into the 'Client' table
for _ in range(10):
    cursor.execute("INSERT INTO Client (name, email, phone, default_address) VALUES (%s, %s, %s, %s)",
                   (fake.name(), fake.email(), fake.phone_number(), random.randint(1, 10)))

# Insert 10 fake elements into the 'client_address' table
for _ in range(10):
    cursor.execute("INSERT INTO client_address (client_id, address_id) VALUES (%s, %s)",
                   (random.randint(1, 10), random.randint(1, 10)))

# Insert 10 fake elements into the 'order' table
for _ in range(10):
    cursor.execute("INSERT INTO `order` (client_id, delivery_address, billing_address, payment_method, order_state) "
                   "VALUES (%s, %s, %s, %s, %s)",
                   (random.randint(1, 10), random.randint(1, 10), random.randint(1, 10),
                    random.choice(['credit_card', 'bank_check', 'cash_on_delivery']),
                    random.choice(['cart', 'validated', 'sent', 'delivery'])))

# Insert 10 fake elements into the 'product' table
for _ in range(10):
    cursor.execute("INSERT INTO product (name, description, images, price, available) "
                   "VALUES (%s, %s, %s, %s, %s)",
                   (fake.word(), fake.text(), fake.image_url(), fake.random_number(2), random.choice([True, False])))

# Insert 10 fake elements into the 'payment_method' table
for _ in range(10):
    cursor.execute("INSERT INTO payment_method (credit_card, bank_check_on_delivery, cash_on_delivery) "
                   "VALUES (%s, %s, %s)",
                   (random.choice([True, False]), random.choice([True, False]), random.choice([True, False])))

# Insert 10 fake elements into the 'order_state' table
for _ in range(10):
    cursor.execute("INSERT INTO order_state (cart, validated, sent, delivery) VALUES (%s, %s, %s, %s)",
                   (random.choice([True, False]), random.choice([True, False]),
                    random.choice([True, False]), random.choice([True, False])))

# Insert 10 fake elements into the 'order_line' table
for _ in range(10):
    cursor.execute("INSERT INTO order_line (product_id, order_id, qt) VALUES (%s, %s, %s)",
                   (random.randint(1, 10), random.randint(1, 10), random.randint(1, 5)))

# Commit the changes and close the connection
connection.commit()
connection.close()




