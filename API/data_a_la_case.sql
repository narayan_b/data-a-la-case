drop DATABASE if exists data_a_la_case;
CREATE DATABASE IF NOT EXISTS data_a_la_case;
USE data_a_la_case;

CREATE TABLE address (
  id INT AUTO_INCREMENT PRIMARY KEY,
  street VARCHAR(255),
  city VARCHAR(255),
  zipcode INT,
  state VARCHAR(255),
  country VARCHAR(255)
);

CREATE TABLE Client (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name varchar(255),
  email varchar(255),
  phone varchar(255),
  default_address INT,
  INDEX (default_address),
  FOREIGN KEY (default_address) REFERENCES address (id)
);

CREATE TABLE client_address (
  id INT AUTO_INCREMENT PRIMARY KEY,
  client_id INT,
  address_id INT,
  INDEX (client_id),
  INDEX (address_id),
  FOREIGN KEY (client_id) REFERENCES Client (id),
  FOREIGN KEY (address_id) REFERENCES address (id)
);

CREATE TABLE `order` (
  id INT AUTO_INCREMENT PRIMARY KEY,
  client_id INT, 
  delivery_address INT,
  billing_address INT,
  payment_method ENUM('credit_card', 'bank_check', 'cash_on_delivery'),
  order_state ENUM('cart', 'validated', 'sent', 'delivery'),
  INDEX (delivery_address),
  INDEX (billing_address),
  INDEX (client_id),
  FOREIGN KEY (delivery_address) REFERENCES address (id),
  FOREIGN KEY (billing_address) REFERENCES address (id),
  FOREIGN KEY (client_id) REFERENCES Client (id) 
);

CREATE TABLE product (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name varchar(255),
  description text,
  images text,
  price varchar(255),
  available bool
);

CREATE TABLE payment_method (
  id INT AUTO_INCREMENT PRIMARY KEY,
  credit_card bool,
  bank_check_on_delivery bool,
  cash_on_delivery bool
);

CREATE TABLE order_state (
  id INT AUTO_INCREMENT PRIMARY KEY,
  cart bool,
  validated bool,
  sent bool,
  delivery bool
);

CREATE TABLE order_line (
  id INT AUTO_INCREMENT PRIMARY KEY,
  product_id INT,
  order_id INT,
  qt INT,
  INDEX (product_id),
  INDEX (order_id),
  FOREIGN KEY (product_id) REFERENCES product (id),
  FOREIGN KEY (order_id) REFERENCES `order` (id)
);

