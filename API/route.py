import mysql.connector
from fastapi import FastAPI
from http_exceptions import HTTPException
from fastapi.responses import JSONResponse

app = FastAPI()

# Establish the database connection
connection = mysql.connector.connect(
    host="localhost",
    user="narayan23",
    password="coucou123",
    auth_plugin='mysql_native_password',
    database='data_a_la_case'
)

# Create a cursor for executing SQL queries
cursor = connection.cursor()

# Example: Run a simple query to check connection
cursor.execute("SELECT 1;")
result = cursor.fetchone()

# Function to execute SQL queries and return results as JSON
def execute_query(query):
    cursor.execute(query)
    columns = [col[0] for col in cursor.description]
    data = cursor.fetchall()
    return [dict(zip(columns, row)) for row in data]


# Get all the elements from the table
@app.get("/")
def read_root():
    return {"message": "Hello, World!"}

# Route to get a list of all products in JSON format
@app.get("/product")
def get_all_products():
    query = "SELECT * FROM product;"
    return execute_query(query)


# Route to get a specific product by ID in JSON format
@app.get("/product/{product_id}")
def get_product_by_id(product_id: int):
    query = f"SELECT * FROM product WHERE id = {product_id};"
    result = execute_query(query)
    if not result:
        raise HTTPException(status_code=404, detail={"message": "Product not found"})
    return result[0]


# Route to add a new product
@app.post("/product")
def create_product(product: dict):
    name = product.get("name", "")
    description = product.get("description", "")
    images = product.get("images", "")
    price = product.get("price", "")
    available = product.get("available", False)

    query = f"INSERT INTO product (name, description, images, price, available) " \
            f"VALUES ('{name}', '{description}', '{images}', '{price}', {available});"

    cursor.execute(query)
    connection.commit()

    return {"message": "Product added successfully"}


# Route to update a product by ID
@app.post("/product/{product_id}")
def update_product(product_id: int, product: dict):
    name = product.get("name", "")
    description = product.get("description", "")
    images = product.get("images", "")
    price = product.get("price", "")
    available = product.get("available", False)

    query = f"UPDATE product SET name = '{name}', description = '{description}', " \
            f"images = '{images}', price = '{price}', available = {available} " \
            f"WHERE id = {product_id};"

    cursor.execute(query)
    connection.commit()
    return {"message": "Product updated successfully"}


# Route to delete a product by ID
@app.delete("/product/{product_id}")
def delete_product(product_id: int):
    # Delete associated order lines
    delete_order_lines_query = f"DELETE FROM order_line WHERE product_id = {product_id};"
    cursor.execute(delete_order_lines_query)

    # Delete the product
    delete_product_query = f"DELETE FROM product WHERE id = {product_id};"
    cursor.execute(delete_product_query)
    connection.commit()

    return {"message": "Product deleted successfully"}


# Route to get a list of all clients in JSON format
@app.get("/client")
def get_all_clients():
    query = "SELECT * FROM Client;"
    return execute_query(query)


# Route to get a specific client by ID in JSON format
@app.get("/client/{client_id}")
def get_client_by_id(client_id: int):
    query = f"SELECT * FROM Client WHERE id = {client_id};"
    result = execute_query(query)
    if not result:
        raise HTTPException(status_code=404, detail="Client not found")
    return result[0]


# Route to add a new client
@app.post("/client")
def create_client(client: dict):
    name = client.get("name", "")
    email = client.get("email", "")
    phone = client.get("phone", "")
    default_address = client.get("default_address", None)

    query = f"INSERT INTO Client (name, email, phone, default_address) " \
            f"VALUES ('{name}', '{email}', '{phone}', {default_address});"

    cursor.execute(query)
    connection.commit()

    return {"message": "Client added successfully"}


# Route to update a client by ID
@app.post("/client/{client_id}")
def update_client(client_id: int, client: dict):
    name = client.get("name", "")
    email = client.get("email", "")
    phone = client.get("phone", "")
    default_address = client.get("default_address", None)

    query = f"UPDATE Client SET name = '{name}', email = '{email}', " \
            f"phone = '{phone}', default_address = {default_address} " \
            f"WHERE id = {client_id};"

    cursor.execute(query)
    connection.commit()

    return {"message": "Client updated successfully"}


# Route to get a list of addresses associated with a client by ID
@app.get("/client/{client_id}/adresses")
def get_client_addresses(client_id: int):
    query = f"SELECT * FROM client_address WHERE client_id = {client_id};"
    return execute_query(query)

# Route to delete a client by ID
@app.delete("/client/{client_id}")
def delete_client(client_id: int):
    query = f"DELETE FROM Client WHERE id = {client_id};"
    cursor.execute(query)
    connection.commit()
    return {"message": "Client deleted successfully"}


# Route to create a new order and return its id
@app.put("/order")
def create_order():
    query = "INSERT INTO `order` (client_id, delivery_address, billing_address, payment_method, order_state) " \
            "VALUES (NULL, NULL, NULL, NULL, NULL);"
    cursor.execute(query)
    connection.commit()

    # Get the last inserted order id
    cursor.execute("SELECT LAST_INSERT_ID();")
    result = cursor.fetchone()
    order_id = result[0]

    return {"order_id": order_id}


# Route to get details of an order by ID
@app.get("/order/{order_id}")
def get_order_by_id(order_id: int):
    # Query to get order details, order lines, addresses, and client information
    query = f"SELECT o.*, c.*, oa.*, ba.* " \
            f"FROM `order` o " \
            f"LEFT JOIN Client c ON o.Client_id = c.id " \
            f"LEFT JOIN address oa ON o.delivery_address = oa.id " \
            f"LEFT JOIN address ba ON o.billing_address = ba.id " \
            f"WHERE o.id = {order_id};"
    result = execute_query(query)

    if not result:
        raise HTTPException(status_code=404, detail="Order not found")

    return result[0]


# Route to get a list of all orders
@app.get("/order")
def get_all_orders():
    query = "SELECT * FROM `order`;"
    return execute_query(query)


# Route to get a list of order ids by client ID
@app.get("/order/byClient/{client_id}")
def get_orders_by_client(client_id: int):
    query = f"SELECT id FROM `order` WHERE client_id = {client_id};"
    return execute_query(query)


# Route to add or update a line in an order
@app.put("/order/{order_id}/{product_id}/{qt}")
def add_or_update_order_line(order_id: int, product_id: int, qt: int):
    # Check if the order and product exist
    query = f"SELECT * FROM `order` WHERE id = {order_id};"
    result = execute_query(query)
    if not result:
        return JSONResponse(status_code=404, content={"detail": "Order not found"})

    query = f"SELECT * FROM product WHERE id = {product_id};"
    result = execute_query(query)
    if not result:
        return JSONResponse(status_code=404, content={"detail": "Product not found"})

    # Add or update the order line
    query = f"INSERT INTO order_line (product_id, order_id, qt) " \
            f"VALUES ({product_id}, {order_id}, {qt}) " \
            f"ON DUPLICATE KEY UPDATE qt = {qt};"
    cursor.execute(query)
    connection.commit()

    return {"message": "Order line added or updated successfully"}

# Route to assign a command to a client
@app.put("/order/{order_id}/{client_id}")
def assign_order_to_client(order_id: int, client_id: int):
    # Check if the order and client exist
    query = f"SELECT * FROM `order` WHERE id = {order_id};"
    result = execute_query(query)
    if not result:
        raise HTTPException(status_code=404, detail="Order not found")

    query = f"SELECT * FROM Client WHERE id = {client_id};"
    result = execute_query(query)
    if not result:
        raise HTTPException(status_code=404, detail="Client not found")

    # Check if the order is already assigned to a client
    if result[0]["client_id"] is not None:
        raise HTTPException(status_code=400, detail="Order is already assigned to a client")

    # Assign the order to the client
    query = f"UPDATE `order` SET client_id = {client_id} WHERE id = {order_id};"
    cursor.execute(query)
    connection.commit()

    return {"message": f"Order {order_id} assigned to client {client_id}"}


# Route to update the delivery address of an order
@app.put("/order/delivery/{order_id}")
def update_order_delivery_address(order_id: int, delivery: int):
    # Check if the order exists
    query = f"SELECT * FROM `order` WHERE id = {order_id};"
    result = execute_query(query)
    if not result:
        raise HTTPException(status_code=404, detail="Order not found")

    # Check if a client is associated with the order
    if result[0]["client_id"] is not None:
        # Check if the address belongs to the associated client
        query = f"SELECT * FROM client_address WHERE client_id = {result[0]['client_id']} AND address_id = {delivery};"
        address_result = execute_query(query)
        if not address_result:
            raise HTTPException(status_code=400, detail="Delivery address does not belong to the client")

    # Update the delivery address of the order
    query = f"UPDATE `order` SET delivery_address = {delivery} WHERE id = {order_id};"
    cursor.execute(query)
    connection.commit()

    return {"message": "Delivery address updated successfully"}


# Route to update the billing address of an order
@app.put("/order/billing/{order_id}")
def update_order_billing_address(order_id: int, billing: int):
    # Check if the order exists
    query = f"SELECT * FROM `order` WHERE id = {order_id};"
    result = execute_query(query)
    if not result:
        raise HTTPException(status_code=404, detail="Order not found")

    # Check if a client is associated with the order
    if result[0]["client_id"] is not None:
        # Check if the address belongs to the associated client
        query = f"SELECT * FROM client_address WHERE client_id = {result[0]['client_id']} AND address_id = {billing};"
        address_result = execute_query(query)
        if not address_result:
            raise HTTPException(status_code=400, detail="Billing address does not belong to the client")

    # Update the billing address of the order
    query = f"UPDATE `order` SET billing_address = {billing} WHERE id = {order_id};"
    cursor.execute(query)
    connection.commit()

    return {"message": "Billing address updated successfully"}


# Route to update the payment method of an order
@app.put("/order/payment/{order_id}")
def update_order_payment_method(order_id: int, payment: str):
    # Check if the order exists
    query = f"SELECT * FROM `order` WHERE id = {order_id};"
    result = execute_query(query)
    if not result:
        raise HTTPException(status_code=404, detail="Order not found")

    # Update the payment method of the order
    query = f"UPDATE `order` SET payment_method = '{payment}' WHERE id = {order_id};"
    cursor.execute(query)
    connection.commit()

    return {"message": "Payment method updated successfully"}


# Route to get the next state of an order
@app.get("/order/{order_id}/next")
def get_next_order_state(order_id: int):
    # Check if the order exists
    query = f"SELECT * FROM `order` WHERE id = {order_id};"
    result = execute_query(query)
    if not result:
        raise HTTPException(status_code=404, detail="Order not found")

    # Get the current state of the order
    current_state = result[0]["order_state"]

    # Define the order state transitions
    state_transitions = {
        "cart": "validated",
        "validated": "sent",
        "sent": "delivery",
        "delivery": "completed"
    }

    # Check if the current state allows a transition
    if current_state not in state_transitions:
        raise HTTPException(status_code=400, detail="Invalid state transition for the order")

    # Update the order state to the next state
    next_state = state_transitions[current_state]
    query = f"UPDATE `order` SET order_state = '{next_state}' WHERE id = {order_id};"
    cursor.execute(query)
    connection.commit()

    return {"message": f"Order {order_id} state transitioned to {next_state}"}


# Route to add or update a line in an order
@app.put("/order/{order_id}/{product_id}/{qt}")
def add_or_update_order_line(order_id: int, product_id: int, qt: int):
    # Check if the order and product exist
    query = f"SELECT * FROM `order` WHERE id = {order_id};"
    result = execute_query(query)
    if not result:
        raise HTTPException(status_code=404, detail="Order not found")

    query = f"SELECT * FROM product WHERE id = {product_id};"
    result = execute_query(query)
    if not result:
        raise HTTPException(status_code=404, detail="Product not found")

    # Add or update the order line
    query = f"INSERT INTO order_line (product_id, order_id, qt) " \
            f"VALUES ({product_id}, {order_id}, {qt}) " \
            f"ON DUPLICATE KEY UPDATE qt = {qt};"
    cursor.execute(query)
    connection.commit()

    return {"message": "Order line added or updated successfully"}


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="127.0.0.1", port=8000, reload=True)